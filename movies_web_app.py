import os

from flask import *
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application

db = os.environ.get("DB", None) or os.environ.get("database", None)
username = os.environ.get("USER", None) or os.environ.get("username", None)
password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)


cnx = ''
try:
	cnx = mysql.connector.connect(user=username, password=password,
                                  host=hostname,
                                  database=db)
except Exception as exp:
	print(exp)
	import MySQLdb
    #try:
	cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    #except Exception as exp1:
    #    print(exp1)

print("Create Table")
cur = cnx.cursor(buffered=True)
table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year varchar(20), title varchar(80), director varchar(40), actor varchar(40), release_date varchar(30), rating FLOAT, PRIMARY KEY (id))'
try:
	cur.execute(table_ddl)
	cnx.commit()
	print("CREATED")

except mysql.connector.Error as err:
	print("NOT CREATED")

	if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
		print("already exists.")
	else:
		print(err.msg)

@application.route('/')
def main_page():
	return render_template('index.html')

@application.route('/add_movie', methods=["POST"])
def add_movie():
	if request.method == 'POST':
		year = request.form['year']
		title = request.form['title'].lower()
		director = request.form['director'].lower() 
		actor = request.form['actor'].lower()
		release_date = request.form['release_date']
		rating = request.form['rating']



		try:
			cur.execute("SELECT year, title, director, actor, release_date, rating FROM movies WHERE title='" + title + "'")
			movies = cur.fetchall()
			if len(movies) != 0:
				return_message = "Movie " + request.form['title'] + " could not be inserted: Movie already exists!"
				return render_template('index.html', message=return_message)

			cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES ('"+
				year + "', '" + title + "', '" + director + "', '" + actor + "', '" + release_date + "', '" + rating + "')")
			cnx.commit()
			return_message = "Movie " + request.form['title'] + " successfully inserted"

		except mysql.connector.Error as err:
			return_message = "Movie " + request.form['title'] + " could not be inserted: " + err.msg
		
		return render_template('index.html', message=return_message)
	return redirect('/')

@application.route('/update_movie', methods=["POST"])
def update_movie():
	if request.method == 'POST':

		cur.execute("SELECT year, title, director, actor, release_date, rating FROM movies WHERE title='" + request.form['title'].lower() + "'")
		movie = cur.fetchall()
		if len(movie) == 0:
			return_message = "Movie " + request.form['title'] + " could not be updated: Movie does not exist!"
			return render_template('index.html', message=return_message)
		year = request.form['year']
		title = request.form['title'].lower()
		director = request.form['director'].lower()
		actor = request.form['actor'].lower()
		release_date = request.form['release_date']
		rating = request.form['rating']

		try:
			cur.execute("UPDATE movies SET director='"+ director + "', actor='"+ actor + "', release_date='"+ release_date + "', rating='" + rating + "' WHERE title='" + title + "'")		
			cnx.commit()
			return_message = "Movie " + request.form['title'] + " successfully updated"

		except mysql.connector.Error as err:
			return_message = "Movie " + request.form['title'] + " could not be updated: " + err.msg
		return render_template('index.html', message=return_message)
	return redirect('/')
	
@application.route('/delete_movie', methods=["POST"])
def delete_movie():
	if request.method == 'POST':
		cur.execute("DELETE FROM movies WHERE title='" + request.form['delete_title'].lower() + "'")
		cnx.commit()
		if cur.rowcount == 0:
			return_message = "Movie " + request.form['delete_title'] + " could not be deleted: Movie does not exist!"
			return render_template('index.html', message=return_message)
	return_message = "Movie " + request.form['delete_title'] + " successfully deleted"
	return render_template('index.html', message=return_message)

@application.route('/search_movie', methods=["GET"])
def search_movie():
	if request.method == 'GET':
		cur.execute("SELECT year, title, director, actor, release_date, rating FROM movies WHERE actor='" + request.args.get('search_actor').lower() +"'")
		movies = cur.fetchall()
		if len(movies) == 0:
			return_message = "No movies found for actor " + request.args.get('search_actor')
			return render_template('results.html', message=return_message)
	return render_template('results.html', movies=movies)

@application.route('/highest_rating', methods=["GET"])
def highest_rating():
	if request.method == 'GET':
		cur.execute("SELECT year, title, director, actor, release_date, rating FROM movies WHERE rating=(SELECT MAX(rating) FROM movies)")
		movies = cur.fetchall()
		print(movies)
	return render_template('results.html', movies=movies)	

@application.route('/lowest_rating', methods=["GET"])
def lowest_rating():
	if request.method == 'GET':
		cur.execute("SELECT year, title, director, actor, release_date, rating FROM movies WHERE rating=(SELECT MIN(rating) FROM movies)")
		movies = cur.fetchall()
	return render_template('results.html', movies=movies)

if __name__ == "__main__":
    app.run(host='0.0.0.0')